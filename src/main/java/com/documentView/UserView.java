package com.documentView;

import java.util.List;

public class UserView {

  private String firstName;
  private String lastName;
  private Integer age;
  private String city;
  private List<LocationView> userLocationView;

  public UserView() {
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public List<LocationView> getUserLocationView() {
    return userLocationView;
  }

  public void setUserLocationView(List<LocationView> userLocationView) {
    this.userLocationView = userLocationView;
  }
}
