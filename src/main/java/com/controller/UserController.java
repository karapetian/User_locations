package com.controller;

import com.documentDto.LocationDto;
import com.documentDto.UserCreateDto;
import com.documentView.LocationView;
import com.documentView.UserView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.service.UserService;
import com.documentDto.UserDto;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity addUser(@RequestBody UserCreateDto userDto) {
        String id = userService.create(userDto);
        return new ResponseEntity(id, HttpStatus.CREATED);
    }


    @GetMapping(path = "/{userId}")
    public ResponseEntity getUser(@PathVariable("userId") String id) {
        UserDto userDto = userService.getById(id);
        UserView userView = new UserView();
        userView.setFirstName(userDto.getFirstName());
        userView.setLastName(userDto.getLastName());
        userView.setAge(userDto.getAge());
        userView.setCity(userDto.getCity());

        List<LocationView> locationViewList = new ArrayList<>();
        LocationView locationView = new LocationView();
        List<LocationDto> locationDtoList = userDto.getUserLocationDto();
        for (int i = 0; i < locationDtoList.size(); i++) {
            locationView.setCountry(locationDtoList.get(i).getCountry());
            locationView.setDate(locationDtoList.get(i).getDate());
            locationViewList.add(i, locationView);
        }
        userView.setUserLocationView(locationViewList);
        return new ResponseEntity(userView, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity getAllUsers() {
        List<UserDto> userDtoList = userService.getAll();
        List<UserView> userViewList = new ArrayList<>();
        for (int i = 0; i < userDtoList.size(); i++) {
            UserView userView = new UserView();
            userView.setFirstName(userDtoList.get(i).getFirstName());
            userView.setLastName(userDtoList.get(i).getLastName());
            userView.setAge(userDtoList.get(i).getAge());
            userView.setCity(userDtoList.get(i).getCity());

            List<LocationDto> locationDtoList = userDtoList.get(i).getUserLocationDto();
            List<LocationView> locationViewList = new ArrayList<>();
            for (int k = 0; k < locationDtoList.size(); k++) {
                LocationView locationView = new LocationView();
                locationView.setCountry(locationDtoList.get(k).getCountry());
                locationView.setDate(locationDtoList.get(k).getDate());
                locationViewList.add(k, locationView);
            }
            userView.setUserLocationView(locationViewList);
            userViewList.add(i, userView);
        }
        return new ResponseEntity(userViewList, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{userId}")
    public ResponseEntity deleteUser(@PathVariable("userId") String id) {
        userService.deleteById(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    @PutMapping(path = "/{userId}")
    public ResponseEntity updateUser(@PathVariable("userId") String id, @RequestBody UserView userView) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(userView.getFirstName());
        userDto.setLastName(userView.getLastName());
        userDto.setAge(userView.getAge());
        userDto.setCity(userView.getCity());

        List<LocationView> locationViewList = userView.getUserLocationView();
        LocationDto locationDto = new LocationDto();
        List<LocationDto> locationDtoList = new ArrayList<>();
        for (int k = 0; k < locationViewList.size(); k++) {
            locationDto.setCountry(locationViewList.get(k).getCountry());
            locationDto.setDate(locationViewList.get(k).getDate());
            locationDtoList.add(k, locationDto);
        }
        userDto.setUserLocationDto(locationDtoList);
        UserDto updatedUserDto = userService.update(id, userDto);

        UserView updatedUserView = new UserView();
        updatedUserView.setFirstName(updatedUserDto.getFirstName());
        updatedUserView.setLastName(updatedUserDto.getLastName());
        updatedUserView.setAge(updatedUserDto.getAge());
        updatedUserView.setCity(updatedUserDto.getCity());

        List<LocationView> locationViewListUpdated = new ArrayList<>();
        LocationView locationViewUpdated = new LocationView();
        for (int k = 0; k < locationDtoList.size(); k++) {
            locationViewUpdated.setCountry(locationDtoList.get(k).getCountry());
            locationViewUpdated.setDate(locationDtoList.get(k).getDate());
            locationViewListUpdated.add(k, locationViewUpdated);
        }
        updatedUserView.setUserLocationView(locationViewListUpdated);

        return new ResponseEntity(updatedUserView, HttpStatus.OK);
    }

    @GetMapping(path = "/{userId}/locations")
    public ResponseEntity getUserLocations(@PathVariable("userId") String id) {
        UserDto userDto = userService.getById(id);
        List<LocationDto> userDtoLocations = userDto.getUserLocationDto();
        List<LocationView> locationViewList = new ArrayList<>();
        for (int k = 0; k < userDtoLocations.size(); k++) {
            LocationView locationView = new LocationView();
            locationView.setCountry(userDtoLocations.get(k).getCountry());
            locationView.setDate(userDtoLocations.get(k).getDate());
            locationViewList.add(k, locationView);
        }
        return new ResponseEntity(locationViewList, HttpStatus.OK);
    }

    @PutMapping(path = "/{userId}/locations")
    public ResponseEntity setUserLocation(@PathVariable("userId") String userId, @RequestBody LocationView locationView) {
        LocationDto locationDto = new LocationDto();
        locationDto.setCountry(locationView.getCountry());
        locationDto.setDate(locationView.getDate());

        List<LocationDto> locationDtoList = userService.setUserLocation(userId, locationDto);
        List<LocationView> locationViewList = new ArrayList<>();

        locationDtoList.forEach(lDto -> {
            LocationView locView = new LocationView();
            locView.setCountry(lDto.getCountry());
            locView.setDate(lDto.getDate());
            locationViewList.add(locView);
        });

        return new ResponseEntity(locationViewList, HttpStatus.OK);
    }

    //http://localhost:8080/users/locations/France?startDate=2014-05-21&endDate=2017-05-21
    @GetMapping(path = "/locations/{country}")
    public ResponseEntity getByDate(@PathVariable("country") String country, @RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate ) {
        LocalDate localDateStart = LocalDate.parse(startDate);
        LocalDate localDateEnd = LocalDate.parse(endDate);

        List<UserDto> userDtoList = userService.findByLocations_CountryAndLocations_DateBetween(country, localDateStart, localDateEnd);
        List<UserView> userViewList = new ArrayList<>();
        userDtoList.forEach(userDto -> {
            UserView userView = new UserView();
            userView.setFirstName(userDto.getFirstName());
            userView.setLastName(userDto.getFirstName());
            userView.setAge(userDto.getAge());
            userView.setCity(userDto.getCity());
            userViewList.add(userView);

            List<LocationDto> locationDtoList = userDto.getUserLocationDto();
            List<LocationView> locationViewList = new ArrayList<>();
            locationDtoList.forEach(locationDto -> {
                LocationView locationView = new LocationView();
                locationView.setCountry(locationDto.getCountry());
                locationView.setDate(locationDto.getDate());
                locationViewList.add(locationView);
            });
            userView.setUserLocationView(locationViewList);
        });
        return new ResponseEntity(userViewList, HttpStatus.OK);
    }
}


