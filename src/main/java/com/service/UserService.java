package com.service;

import com.document.User;
import com.documentDto.LocationDto;
import com.documentDto.UserCreateDto;
import com.documentDto.UserDto;
import com.documentView.UserView;

import java.time.LocalDate;
import java.util.List;

public interface UserService {

  String create(UserCreateDto createDto);

  UserDto getById(String id);

  void deleteById(String id);

  UserDto update(String id, UserDto userDto);

  List<UserDto> getAll();

  List<LocationDto> setUserLocation(String id, LocationDto locationDto);

  List<UserDto> findByLocations_CountryAndLocations_DateBetween(String country, LocalDate localDateStart, LocalDate localDateEnd);

}
