package com.service.implementation;

import com.document.LocationDocument;
import com.document.User;
import com.documentDto.LocationDto;
import com.repository.UserRepository;
import com.service.UserService;
import com.documentDto.UserCreateDto;
import com.documentDto.UserDto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public String create(UserCreateDto userCreateDto) {
    User user = new User();
    user.setAge(userCreateDto.getAge());
    user.setCity(userCreateDto.getCity());
    user.setFirstName(userCreateDto.getFirstName());
    user.setLastName(userCreateDto.getLastName());
    user.setLocations(userCreateDto.getUserLocationDocument());
    userRepository.save(user);
    return user.getId();
  }

  @Override
  public UserDto getById(String id) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (!optionalUser.isPresent()) {
      throw new IllegalArgumentException("User not found with the given id " + id);
    }
    UserDto userDto = new UserDto();
    userDto.setFirstName(optionalUser.get().getFirstName());
    userDto.setLastName(optionalUser.get().getLastName());
    userDto.setAge(optionalUser.get().getAge());
    userDto.setCity(optionalUser.get().getCity());

    List<LocationDocument> locationDocumentList = optionalUser.get().getLocations();
    List<LocationDto> locationDtoList = new ArrayList<>();
    for (int i = 0; i < locationDocumentList.size(); i++) {
      LocationDto locationDto = new LocationDto();
      locationDto.setCountry(locationDocumentList.get(i).getCountry());
      locationDto.setDate(locationDocumentList.get(i).getDate());
      locationDtoList.add(i, locationDto);
    }
    userDto.setUserLocationDto(locationDtoList);
    return userDto;
  }

  @Override
  public List<UserDto> getAll() {
    List<User> usersList = userRepository.findAll();
    List<UserDto> userDtoList = new ArrayList<>();
    for (int i = 0; i < usersList.size(); i++) {
      UserDto userDto = new UserDto();
      userDto.setFirstName(usersList.get(i).getFirstName());
      userDto.setLastName(usersList.get(i).getLastName());
      userDto.setAge(usersList.get(i).getAge());
      userDto.setCity(usersList.get(i).getCity());

      List<LocationDocument> locationDocumentList = usersList.get(i).getLocations();
      List<LocationDto> locationDtoList = new ArrayList<>();
      for (int k = 0; k < locationDocumentList.size(); k++) {
        LocationDto locationDto = new LocationDto();
        locationDto.setCountry(locationDocumentList.get(k).getCountry());
        locationDto.setDate(locationDocumentList.get(k).getDate());
        locationDtoList.add(k, locationDto);
      }
      userDto.setUserLocationDto(locationDtoList);
      userDtoList.add(i, userDto);
    }
    return userDtoList;
  }

  @Override
  public void deleteById(String id) {
    userRepository.deleteById(id);
  }

  @Override
  public UserDto update(String id, UserDto userDto) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (!optionalUser.isPresent()) {
      throw new IllegalArgumentException("User not found with the given id " + id);
    }
    User currentUser = optionalUser.get();
    currentUser.setFirstName(userDto.getFirstName());
    currentUser.setLastName(userDto.getLastName());
    currentUser.setAge(userDto.getAge());
    currentUser.setCity(userDto.getCity());

    List<LocationDto> locationDtoList = userDto.getUserLocationDto();
    LocationDocument locationDocument = new LocationDocument();
    List<LocationDocument> locationDocumentList = new ArrayList<>();
    for (int k = 0; k < locationDtoList.size(); k++) {
      locationDocument.setCountry(locationDtoList.get(k).getCountry());
      locationDocument.setDate(locationDtoList.get(k).getDate());
      locationDocumentList.add(k, locationDocument);
    }
    currentUser.setLocations(locationDocumentList);
    userRepository.save(currentUser);
    return userDto;
  }

  @Override
  public List<LocationDto> setUserLocation(String id, LocationDto locationDto) {
    Optional<User> byId = userRepository.findById(id);
    if (!byId.isPresent()) {
      throw new IllegalArgumentException("User not found with the given id " + id);
    }
    User user = byId.get();
    LocationDocument newLocation = new LocationDocument();
    newLocation.setCountry(locationDto.getCountry());
    newLocation.setDate(locationDto.getDate());
    user.getLocations().add(newLocation);
    userRepository.save(user);

    List<LocationDto> locationDtoList = new ArrayList<>();
    List<LocationDocument> locationDocumentList = user.getLocations();
    for (int i = 0; i < locationDocumentList.size(); i++) {
      LocationDto locationDto2 = new LocationDto();
      locationDto2.setCountry(locationDocumentList.get(i).getCountry());
      locationDto2.setDate(locationDocumentList.get(i).getDate());
      locationDtoList.add(i, locationDto2);
    }
    return locationDtoList;
  }

  @Override
  public List<UserDto> findByLocations_CountryAndLocations_DateBetween(String country, LocalDate localDateStart, LocalDate localDateEnd) {
    List<User> userList = userRepository.findByLocations_CountryAndLocations_DateBetween(country, localDateStart, localDateEnd);
    List<UserDto> userDtoList = new ArrayList<>();
    userList.forEach(user ->{
      UserDto userDto = new UserDto();
      userDto.setFirstName(user.getFirstName());
      userDto.setLastName(user.getFirstName());
      userDto.setAge(user.getAge());
      userDto.setCity(user.getCity());
      userDtoList.add(userDto);

      List<LocationDocument> locationDocumentList = user.getLocations();
      List<LocationDto> locationDtoList = new ArrayList<>();
      locationDocumentList.forEach(locationDocument -> {
        LocationDto locationDto = new LocationDto();
        locationDto.setCountry(locationDocument.getCountry());
        locationDto.setDate(locationDocument.getDate());
        locationDtoList.add(locationDto);
      });
      userDto.setUserLocationDto(locationDtoList);
    });
    return userDtoList;
  }
}
