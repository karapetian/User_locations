package com.repository;

import com.document.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByLocations_CountryAndLocations_DateBetween(String country, LocalDate localDateStart, LocalDate localDateEnd);

}
