package com.documentDto;

import com.document.LocationDocument;
import java.util.List;

public class UserCreateDto {

  private String firstName;
  private String lastName;
  private Integer age;
  private String city;
  private List<LocationDocument> userLocationDocument;




  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public List<LocationDocument> getUserLocationDocument() {
    return userLocationDocument;
  }

  public void setUserLocationDocument(List<LocationDocument> userLocationDocument) {
    this.userLocationDocument = userLocationDocument;
  }
}
