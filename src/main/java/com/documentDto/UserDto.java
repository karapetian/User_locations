package com.documentDto;

import com.document.LocationDocument;
import java.util.List;

public class UserDto {

  private String firstName;
  private String lastName;
  private Integer age;
  private String city;
  private List<LocationDto> userLocationDtoes;

  public UserDto() {
  }

  public UserDto(String firstName, String lastName, Integer age, String city,
    List<LocationDto> userLocationDtoes) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.city = city;
    this.userLocationDtoes = userLocationDtoes;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public List<LocationDto> getUserLocationDto() {
    return userLocationDtoes;
  }

  public void setUserLocationDto(List<LocationDto> userLocationDtoes) {
    this.userLocationDtoes = userLocationDtoes;
  }
}
