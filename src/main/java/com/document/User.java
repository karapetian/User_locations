package com.document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private Integer age;
    private String city;
    private List<LocationDocument> locations = new ArrayList<>();

    public User() {
    }

    public User(String id, String firstName, String lastName, Integer age, String city, List<LocationDocument> locations) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.city = city;
        this.locations = locations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<LocationDocument> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationDocument> locations) {
        this.locations = locations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(age, user.age) && Objects.equals(city, user.city);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, age, city);
    }

    @Override
    public String toString() {
        return " id: " + id + "\n firstname: " + firstName + "\n lastname: " + lastName + "\n age: " + age + "\n city: " + city;
    }
}
