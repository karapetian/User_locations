package com.document;

import java.time.LocalDate;

public class LocationDocument {

 private String country;
 private LocalDate date;

  public LocationDocument() {

  }

  public LocationDocument( String country, LocalDate date) {
    this.country = country;
    this.date = date;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }
}
