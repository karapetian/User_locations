package com.controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.UserLocationApplication;
import com.document.LocationDocument;
import com.document.User;
import com.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserLocationApplication.class)
//@EnableMongoRepositories
public class UserControllerTests {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;
    private User userDocument;
    private List<User> userList;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        this.userRepository.deleteAll();
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);
        assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

    @Test
    public void createUser() throws Exception {
        User user = new User();
        user.setFirstName("Varduhi");
        user.setLastName("Hakobyan");
        user.setAge(45);
        user.setCity("Paris");
        LocationDocument locationDocument = new LocationDocument();
        LocalDate localDate = LocalDate.of(2018, 12, 01);
        locationDocument.setDate(localDate);
        locationDocument.setCountry("France");
        user.setLocations(Arrays.asList(locationDocument));

        String userJson = json(user);

        this.mockMvc.perform(post("/users").contentType(contentType).content(userJson)).andExpect(status().isCreated()).andDo(print());
    }

    @Test
    public void getUser() throws Exception {
        User user = new User();
        user.setFirstName("Varduhi");
        user.setLastName("Hakobyan");
        user.setAge(45);
        user.setCity("Paris");
        LocationDocument locationDocument = new LocationDocument();
        LocalDate localDate = LocalDate.of(2018, 12, 01);
        locationDocument.setDate(localDate);
        locationDocument.setCountry("France");
        user.setLocations(Arrays.asList(locationDocument));

        User savedUser = this.userRepository.save(user);
        mockMvc.perform(get("/users/" + savedUser.getId())).andExpect(status().isOk()).andExpect(content().contentType(contentType)).andExpect(jsonPath("$.firstName", is(savedUser.getFirstName()))).andDo(print());
    }

    @Test
    public void getAllUsers() throws Exception{
        User user1 = new User();
        user1.setFirstName("Varduhi");
        user1.setLastName("Hakobyan");
        user1.setAge(45);
        user1.setCity("Paris");
        LocationDocument locationDocument1 = new LocationDocument();
        LocalDate localDate = LocalDate.of(2018, 12, 01);
        locationDocument1.setDate(localDate);
        locationDocument1.setCountry("France");
        user1.setLocations(Arrays.asList(locationDocument1));

        User user2 = new User();
        user2.setFirstName("Hakob");
        user2.setLastName("Hakobyan");
        user2.setAge(45);
        user2.setCity("Berlit");
        LocationDocument locationDocument2 = new LocationDocument();
        LocalDate localDate2 = LocalDate.of(2018, 02, 01);
        locationDocument2.setDate(localDate2);
        locationDocument2.setCountry("France");
        user2.setLocations(Arrays.asList(locationDocument2));

        User savedUser1 = this.userRepository.save(user1);
        User savedUser2 = this.userRepository.save(user2);

        userRepository.findAll();
        mockMvc.perform(get("/users"))
                .andExpect(content().contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$[0].firstName", is(savedUser1.getFirstName())))
                .andExpect(jsonPath("$[1].userLocationView", hasSize(1)))
                .andDo(print());
    }

    @Test
    public void deleteUser() throws Exception {
        User user = new User();
        user.setFirstName("Varduhi");
        user.setLastName("Hakobyan");
        user.setAge(45);
        user.setCity("Paris");
        LocationDocument locationDocument = new LocationDocument();
        LocalDate localDate = LocalDate.of(2018, 12, 01);
        locationDocument.setDate(localDate);
        locationDocument.setCountry("France");
        user.setLocations(Arrays.asList(locationDocument));

        User savedUser = this.userRepository.save(user);

        mockMvc.perform(delete("/users/" + savedUser.getId())).andExpect(status().isOk()).andDo(print());
        Optional<User> userOptional = userRepository.findById(savedUser.getId());
        Assert.assertFalse(userOptional.isPresent());
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
