package com.service;

import static org.junit.Assert.*;

import com.document.LocationDocument;
import com.document.User;
import com.documentDto.LocationDto;
import com.documentDto.UserCreateDto;
import com.documentDto.UserDto;
import com.repository.UserRepository;
import com.service.implementation.UserServiceImpl;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

  @Mock
  private UserRepository userRepository;
  @InjectMocks
  private UserService userService = new UserServiceImpl(userRepository);


  @Test
  public void create() {
    User user = new User();
    String createdUserId = "userID";
    user.setFirstName("Hakob");
    user.setLastName("Hakobyan");
    user.setAge(45);
    user.setCity("Paris");

    LocationDocument locationDocument = new LocationDocument();
    LocalDate localDate = LocalDate.of(2018, 12, 01);
    locationDocument.setDate(localDate);
    locationDocument.setCountry("France");
    user.setLocations(Arrays.asList(locationDocument));

    when(userRepository.save(user)).thenAnswer((Answer<User>) invocation -> {
        User user1 = (User) invocation.getArguments()[0];
        user1.setId(createdUserId);
        return user1;
      }
    );

    UserCreateDto userDto = new UserCreateDto();
    userDto.setFirstName("Hakob");
    userDto.setLastName("Hakobyan");
    userDto.setAge(45);
    userDto.setCity("Paris");

    LocationDto locationDto = new LocationDto();
    LocalDate localDate2 = LocalDate.of(2018, 12, 01);
    locationDto.setDate(localDate2);
    locationDto.setCountry("France");
    userDto.setUserLocationDocument(Arrays.asList(locationDocument));

    String id = userService.create(userDto);
    Assert.assertEquals(createdUserId, id);
    Assert.assertNotNull(userDto.getUserLocationDocument());
  }


  @Test
  public void getById() {
    User user = new User();
    user.setId("aaa123");
    user.setFirstName("Hakob");
    user.setLastName("Hakobyan");
    user.setAge(45);
    user.setCity("Paris");

    LocationDocument locationDocument = new LocationDocument();
    LocalDate localDate = LocalDate.of(2018, 12, 01);
    locationDocument.setDate(localDate);
    locationDocument.setCountry("France");
    user.setLocations(Arrays.asList(locationDocument));

    Optional<User> optionalUser = Optional.of(user);
    when(userRepository.findById("aaa123")).thenReturn(optionalUser);
    UserDto userDto = userService.getById("aaa123");

    verify(userRepository).findById("aaa123");
    Assert.assertNotNull(userDto);
    Assert.assertEquals("Testing getById()", Integer.valueOf(45), userDto.getAge());
    Assert.assertEquals(LocalDate.of(2018, 12, 01), userDto.getUserLocationDto().get(0).getDate());
  }

  @Test
  public void getAll() {

  }

  @Test
  public void deleteById() {
    User user = new User();
    user.setFirstName("Hakob");
    user.setLastName("Hakobyan");
    user.setAge(45);
    user.setCity("Paris");

    String createdUserId = "aaa123";
    when(userRepository.save(user)).thenAnswer((Answer<User>) invocation -> {
        User user1 = (User) invocation.getArguments()[0];
        user1.setId(createdUserId);
        return user1;
      }
    );
    userRepository.save(user);
    userService.deleteById("aaa123");
    verify(userRepository).deleteById(user.getId());
  }

  @Test
  public void update() {
    User user = new User();
    user.setFirstName("Hakob");
    user.setLastName("Hakobyan");
    user.setAge(45);
    user.setCity("Paris");
    user.setId("aaa123");

    when(userRepository.findById("aaa123")).thenReturn(Optional.of(user));

    UserDto updatedUserDto = new UserDto();
    updatedUserDto.setFirstName("Poxos");
    updatedUserDto.setLastName("Hakobyan");
    updatedUserDto.setAge(55);
    updatedUserDto.setCity("Paris");
    updatedUserDto.setUserLocationDto(new ArrayList<>());
   userService.update("aaa123", updatedUserDto);

    User expextedUser = new User();
    expextedUser.setFirstName("Poxos");
    expextedUser.setLastName("Hakobyan");
    expextedUser.setAge(55);
    expextedUser.setCity("Paris");
    expextedUser.setId("aaa123");
    verify(userRepository, times(1)).save(expextedUser);
  }

  @Test
  public void setUserLocation() {
  }
}
